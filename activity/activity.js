//Select using querySelector()
let fullNameLabel = document.querySelector("#label-full-name");
let fullNameDisplay = document.querySelector("#full-name-display");
let firstNameLabel = document.querySelector("#label-first-name");
let firstNameInput = document.querySelector("#txt-first-name");
let lastNameLabel = document.querySelector("#label-last-name");
let lastNameInput = document.querySelector("#txt-last-name");
let submitButton = document.querySelector("#submit-btn")



//Adding values to headings
fullNameLabel.innerHTML = "Full Name:";
firstNameLabel.innerHTML = "First Name:";
lastNameLabel.innerHTML = "Last Name:";



//Creating Functions
const displayName = () => {

	fullNameDisplay.innerHTML = `${firstNameInput.value} ${lastNameInput.value}`;

}

const registerName = () => {

	if (firstNameInput.value !== "" && lastNameInput.value !== ""){
		alert(`Thank you for registering ${fullNameDisplay.innerHTML}!`);
		firstNameInput.value = "";
		lastNameInput.value = "";
		fullNameDisplay.innerHTML = "";

		location.href = "https://zuitt.co";

	} else {

		alert(`Please provide both first and last name.`);
	}

}



//Using addEventListener
firstNameInput.addEventListener('keyup', displayName);
lastNameInput.addEventListener('keyup', displayName);
submitButton.addEventListener('click', registerName);