//console.log("Hello World!");
/*
		JS DOM -Javascript Document Object Model

		For CSS, ALl html elements were considered as a box. This is our CSS Box Model. For Javascript, all HTML elements is considered as an object. This is what we call JS Document Object Model.

		Since in JS, all html elements are objects and can be selected, we can manipulate and iteractions to our web page using javascript.

*/

//document is a keyword in JS which refers to the whole document. To the whole html page.
console.log(document);

//store/select a particular element from our page to a variable.
//querySelector() is a method which can be used to select a specific element form our document. The querySelector() uses css-like notation/selector to select an element.
let firstNameLabel = document.querySelector("#label-first-name");

//Stored/selected the element with the id label-first-name. We can then manipulate the element using JS.
console.log(firstNameLabel);



//Mini-Activity:
let lastNameLabel = document.querySelector("#label-last-name");
console.log(lastNameLabel);



//We can access properties from our elements, because JS considers html elements as Objects.
//What is the output of the property .innerHTML from our firstNameLabel? First Name:
//innerHTML is a property of an element which considers all the children of the selected element as string. This includes other elments and text content.
console.log(firstNameLabel.innerHTML);

//can we update the innerHTML property of an element?
firstNameLabel.innerHTML = "I like New York City.";



//Mini-Activity:
lastNameLabel.innerHTML = "My Favorite Food is ."



//Use if else statements to change the text of our label based on a condition.

let city = "Tokyo";

if (city === "New York"){
	firstNameLabel.innerHTML = `I like New York City.`
} else {
	firstNameLabel.innerHTML = `I dont like New York. I like ${city} city.`
}

//Events - allows us to add interactivity to our page. Wherein we can have our users interact with our page and our page can perform a tack.

/*
	Event Listener:
	Event Listeners allow us to listen/observe for user interaction within our page.

	On the event that the user clicks on our firstNameLabel, we ran our anonymous function to perform a task, which is to change the innerHTML of our firstNameLabel to a different message.
*/

firstNameLabel.addEventListener('click', () => {

	firstNameLabel.innerHTML = "I've been clicked. Send help!";
	//ELements have a property called style which is able to style our element. Style property is also a JS object.
	firstNameLabel.style.color = "red";
	firstNameLabel.style.fontSize = "10vh";

});




//Mini-Activity:

lastNameLabel.addEventListener('click',()=>{

/*	lastNameLabel.style.color = "violet";
	lastNameLabel.style.fontSize = "5vh";*/

	//Check the current color of the lastNameLabel
	if(lastNameLabel.style.color === "violet"){
		lastNameLabel.style.color = "black";
		lastNameLabel.style.fontSize = "16px";
	} else {
		lastNameLabel.style.color = "violet";
		lastNameLabel.style.fontSize = "5vh"
	}
})

//keyup - is an event wherein we able to perform a task when the use lets go of a key. Keyup is best in input element that require key inputs.

//select the input element
let inputFirstName = document.querySelector("#txt-first-name");
//.value is a property of mostly input elements. It contains the current value of the element.

//Initially, our input elements are empty.
//So, initially, our .value property for our inputFirstName is blank.
//Why is the .value still blank when we type into the input?
//This console log only outputs and ran the first time our page was shown, therefore, any changes you typed into the input element will not be shown, because console.log() only ran once.
console.log(inputFirstName.value);

//To be able to log in the console, the current value of our input element as we type, we have to add an event.

/*inputFirstName.addEventListener('keyup', ()=>{

	console.log(inputFirstName.value);
	
})*/



//Mini-Activity
let fullNameDisplay = document.querySelector("#full-name-display");
let lastNameInput = document.querySelector("#txt-last-name");

console.log(fullNameDisplay);
console.log(lastNameInput)

/*lastNameInput.addEventListener('keyup', () => {

	console.log(lastNameInput.value);

})*/



//Syntax of addEventListener:
//element.addEventListener(<event>, <function>);

//You can create/add named functions as the function for the addEventListener.

const showName = () => {

	console.log(inputFirstName.value);
	//whenever a user types into our input elements, both the current values of inputFirstName and inputLastName must be logged:
	console.log(lastNameInput.value);
	//console.log(fullNameDisplay.innerHTML);//results to blank because the initially the h3 does not have a text content or children.


	//Update the text content of our h3 witth the current value of our inputFirstName:
	//fullNameDisplay.innerHTML = inputFirstName.value+" "+inputLastName.value;
	fullNameDisplay.innerHTML = `${inputFirstName.value} ${lastNameInput.value}`

}

inputFirstName.addEventListener('keyup', showName);

//Multiple addEventListeners can run the same named function.
lastNameInput.addEventListener('keyup', showName);